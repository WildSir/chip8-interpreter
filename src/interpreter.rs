use std::{
    fs::File,
    io::{self, Read},
    path::{Path, PathBuf},
    sync::mpsc::{SendError, TryRecvError},
};

use log::{error, info};
use serde::{Deserialize, Serialize};
use thiserror::Error;

use crate::{
    instruction::{Instruction, InstructionParseError},
    timers::{DelayTimer, SoundTimer},
    utils, Display, DisplayInstruction, DisplayMethod, Memory,
};

#[derive(Serialize, Deserialize)]
struct SaveState {
    program_path: PathBuf,
    regs: [u8; 16],
    address_reg: usize,
    sound_timer: SoundTimer,
    delay_timer: DelayTimer,
    pub program_counter: usize,
    stack: Vec<usize>,
    memory: Memory,
    program_end: Option<usize>,
    display: Vec<u8>,
}

#[derive(Debug)]
pub struct Interpreter<T> {
    pub display: Display<T>,
    regs: [u8; 16],
    address_reg: usize,
    sound_timer: SoundTimer,
    delay_timer: DelayTimer,
    pub program_counter: usize,
    stack: Vec<usize>,
    memory: Memory,
    program_end: Option<usize>,
}

#[derive(Debug, Error)]
pub enum InterpreterError<T> {
    #[error("Failed to update display")]
    Send(#[from] SendError<DisplayInstruction<T>>),
    #[error("Failed to read keyboard input")]
    TryRecv(#[from] TryRecvError),
    #[error("{0}")]
    InstructionParseError(#[from] InstructionParseError),
    #[error("Failed to load save state: {0}")]
    Load(io::Error, Interpreter<T>),
    #[error("Failed to save state: {0}")]
    Save(#[from] io::Error),
}

#[derive(Debug)]
pub enum ControlFlow {
    // Continue as usual
    Continue,
    // Program has ended
    End,
    // A sprite has been drawn, refresh the screen; continue as usual
    RefreshDisplay,
}

impl<T: DisplayMethod> Interpreter<T> {
    pub fn new(display: Display<T>) -> Self {
        Interpreter {
            display,
            regs: Default::default(),
            address_reg: Default::default(),
            sound_timer: Default::default(),
            delay_timer: Default::default(),
            program_counter: Default::default(),
            stack: Default::default(),
            memory: Default::default(),
            program_end: None,
        }
    }
    pub fn save_state<P: AsRef<Path>>(&self, save_path: P) -> Result<(), InterpreterError<T>> {
        let file = File::create(&save_path)?;
        let save_state: SaveState = SaveState {
            program_path: save_path.as_ref().to_path_buf(),
            display: self.display.get_screen_buf().to_vec(),
            regs: self.regs,
            address_reg: self.address_reg,
            sound_timer: self.sound_timer,
            delay_timer: self.delay_timer,
            program_counter: self.program_counter,
            stack: self.stack.clone(),
            memory: self.memory.clone(),
            program_end: self.program_end,
        };

        match serde_json::to_writer_pretty(file, &save_state) {
            Ok(_) => {}
            Err(err) => return Err(InterpreterError::Save(err.into())),
        }
        Ok(())
    }
    pub fn load_state<P: AsRef<Path>>(mut self, load_path: P) -> Result<Self, InterpreterError<T>> {
        let file = match File::open(&load_path) {
            Ok(file) => file,
            Err(err) => return Err(InterpreterError::Load(err, self)),
        };
        let save_state: SaveState = match serde_json::from_reader(file) {
            Ok(save_state) => save_state,
            Err(err) => return Err(InterpreterError::Load(err.into(), self)),
        };

        // TODO: Return an actual error
        if save_state.program_path != load_path.as_ref() {
            return Ok(self);
        }
        self.display.load_screen_buf(&save_state.display);
        Ok(Self {
            display: self.display,
            regs: save_state.regs,
            address_reg: save_state.address_reg,
            sound_timer: save_state.sound_timer,
            delay_timer: save_state.delay_timer,
            program_counter: save_state.program_counter,
            stack: save_state.stack,
            memory: save_state.memory,
            program_end: save_state.program_end,
        })
    }
    pub fn load_program(&mut self, program: &mut impl Read) -> Result<(), io::Error> {
        let mut bytes = Vec::new();
        program.read_to_end(&mut bytes)?;
        self.memory.write_at(&bytes, 512);
        self.program_counter = 512;
        self.program_end = Some(bytes.len());
        Ok(())
    }
    pub fn memory(&self) -> &[u8] {
        &self.memory.bytes
    }
    pub fn tick_timers(&mut self) {
        self.delay_timer.tick();
        self.sound_timer.tick();
    }
    pub fn run_instruction(&mut self) -> Result<ControlFlow, InterpreterError<T>> {
        match &self.program_end {
            Some(program_end) => {
                if self.program_counter == *program_end {
                    return Ok(ControlFlow::End);
                }
            }
            None => panic!("tried to run interpreter without loading program"),
        }
        let bytes = &self.memory()[self.program_counter..=self.program_counter + 1];
        let opcode = bytes[1] as u16 | ((bytes[0] as u16) << 8);

        let instruction = match Instruction::try_from(opcode) {
            Ok(instruction) => instruction,
            // Maybe use this to determine end
            Err(err) => return Err(err.into()),
        };
        match instruction {
            Instruction::SYS(_) => info!("Ignoring SYS addr instruction"),
            Instruction::CLS => {
                self.display.clear();
            }
            Instruction::RET => {
                if let Some(addr) = self.stack.pop() {
                    self.program_counter = addr;
                }
            }
            Instruction::JP1(pos) => {
                self.program_counter = pos;
                return Ok(ControlFlow::Continue);
            }
            Instruction::CALL(pos) => {
                self.stack.push(self.program_counter);
                self.program_counter = pos;
                return Ok(ControlFlow::Continue);
            }
            Instruction::SE1(x, byte) => {
                if self.regs[x] == byte {
                    self.program_counter += 2;
                }
            }
            Instruction::SNE1(x, byte) => {
                if self.regs[x] != byte {
                    self.program_counter += 2;
                }
            }
            Instruction::SE2(x, y) => {
                if self.regs[x] == self.regs[y] {
                    self.program_counter += 2;
                }
            }
            Instruction::LD1(x, byte) => self.regs[x] = byte,
            Instruction::ADD1(x, byte) => self.regs[x] = self.regs[x].wrapping_add(byte),
            Instruction::LD2(x, y) => self.regs[x] = self.regs[y],
            Instruction::OR(x, y) => {
                self.regs[x] |= self.regs[y];
                // To conform to original Chip-8 Implementation
                self.regs[0xF] = 0;
            }
            Instruction::AND(x, y) => {
                self.regs[x] &= self.regs[y];
                // To conform to original Chip-8 Implementation
                self.regs[0xF] = 0;
            }
            Instruction::XOR(x, y) => {
                self.regs[x] ^= self.regs[y];
                // To conform to original Chip-8 Implementation
                self.regs[0xF] = 0;
            }
            Instruction::ADD2(x, y) => match self.regs[x].checked_add(self.regs[y]) {
                Some(val) => {
                    self.regs[x] = val;
                    self.regs[0xF] = 0;
                }
                None => {
                    self.regs[x] = self.regs[x].wrapping_add(self.regs[y]);
                    self.regs[0xF] = 1;
                }
            },
            Instruction::SUB(x, y) => {
                let temp_x = self.regs[x];
                self.regs[x] = self.regs[x].wrapping_sub(self.regs[y]);
                self.regs[0xF] = if temp_x < self.regs[x] { 0 } else { 1 };
            }
            Instruction::SHR(x, y) => {
                let temp_lsb = self.regs[x] & 1;
                self.regs[x] = self.regs[y].wrapping_shr(1);
                self.regs[0xF] = temp_lsb;
            }
            Instruction::SUBN(x, y) => {
                let temp_y = self.regs[y];
                self.regs[x] = self.regs[y].wrapping_sub(self.regs[x]);
                self.regs[0xF] = if temp_y < self.regs[x] { 0 } else { 1 };
            }
            Instruction::SHL(x, y) => {
                let temp_msb = (self.regs[x] >> 7) & 1;
                self.regs[x] = self.regs[y].wrapping_shl(1);
                self.regs[0xF] = temp_msb;
            }
            Instruction::SNE2(x, y) => {
                if self.regs[x] != self.regs[y] {
                    self.program_counter += 2;
                }
            }
            Instruction::LD3(addr) => {
                self.address_reg = addr;
            }
            Instruction::JP2(addr) => {
                self.program_counter = addr;
            }
            Instruction::RND(x, byte) => {
                self.regs[x] = rand::random::<u8>() & byte;
            }
            Instruction::DRW(x, y, n) => {
                let sprite_data = {
                    let memory_pos = self.address_reg;
                    &self.memory.bytes[memory_pos..memory_pos + n as usize]
                }
                .to_vec();
                self.regs[0xF] = self.display.draw_sprite(
                    &sprite_data,
                    self.regs[x] as usize,
                    self.regs[y] as usize,
                ) as u8;
                self.program_counter += 2;
                return Ok(ControlFlow::RefreshDisplay);
            }
            Instruction::SKP(x) => {
                let keys = self.display.read_keys();
                if let Some(key) = keys.get(&self.regs[x]) {
                    if *key == self.regs[x] {
                        self.program_counter += 2;
                    }
                };
            }
            Instruction::SKNP(x) => {
                match self.display.read_keys().get(&self.regs[x]) {
                    Some(key) => {
                        if *key != self.regs[x] {
                            self.program_counter += 2;
                        }
                    }
                    None => {
                        self.program_counter += 2;
                    }
                };
            }
            Instruction::LD4(x) => self.regs[x] = self.delay_timer.x,
            Instruction::LD5(x) => loop {
                if let Some(key) = self.display.read_keys().get(&self.regs[x]) {
                    self.regs[x] = *key;
                    break;
                }
            },
            Instruction::LD6(x) => self.delay_timer.x = self.regs[x],
            Instruction::LD7(x) => self.sound_timer.x = self.regs[x],
            Instruction::ADD3(x) => self.address_reg += self.regs[x] as usize,
            Instruction::LD8(x) => self.address_reg = 5 * self.regs[x] as usize,
            Instruction::LD9(x) => self
                .memory
                .write_at(&utils::as_bcd(self.regs[x]), self.address_reg),
            Instruction::LD10(x) => {
                for i in 0..=x {
                    self.memory.write_at(&[self.regs[i]], self.address_reg + i);
                }
                // To conform to original Chip-8 Implementation
                self.address_reg += x + 1;
            }
            Instruction::LD11(x) => {
                for i in 0..=x {
                    self.regs[i] = self.memory.bytes[i + self.address_reg];
                }
                // To conform to original Chip-8 Implementation
                self.address_reg += x + 1;
            }
        }
        self.program_counter += 2;
        Ok(ControlFlow::Continue)
    }
}
