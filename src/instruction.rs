use super::utils::{self, combine_nibbles};
use thiserror::Error;

// Instruction set at http://devernay.free.fr/hacks/chip8/C8TECH10.HTM, instruction are mostly in order according to guide
#[derive(Debug)]
pub enum Instruction {
    SYS(usize),
    CLS,
    RET,
    // nnn
    JP1(usize),
    // nnn
    JP2(usize),
    CALL(usize),

    SE1(usize, u8),
    SE2(usize, usize),

    SNE1(usize, u8),
    SNE2(usize, usize),

    LD1(usize, u8),
    LD2(usize, usize),
    LD3(usize),
    LD4(usize),
    // Keypress
    LD5(usize),
    // Delay Timer
    LD6(usize),
    // Sound Timer
    LD7(usize),
    // Hex Sprites
    LD8(usize),
    // BCD
    LD9(usize),
    // Store v0-vX in memory
    LD10(usize),
    // Reads v0-vX from memory
    LD11(usize),

    ADD1(usize, u8),
    ADD2(usize, usize),
    ADD3(usize),

    OR(usize, usize),
    AND(usize, usize),
    XOR(usize, usize),

    SUB(usize, usize),
    SUBN(usize, usize),

    SHL(usize, usize),
    SHR(usize, usize),

    RND(usize, u8),
    DRW(usize, usize, u8),

    SKP(usize),
    SKNP(usize),
}

#[derive(Debug, Error)]
#[error("bytes `{0}` do not match to any instruction")]
pub struct InstructionParseError(u16);

impl TryFrom<u16> for Instruction {
    type Error = InstructionParseError;
    fn try_from(opcode: u16) -> Result<Self, Self::Error> {
        match utils::split_nibbles(opcode) {
            (0, 0, 0xE, 0) => Ok(Instruction::CLS),
            (0, 0, 0xE, 0xE) => Ok(Instruction::RET),
            (0, nibble1, nibble2, nibble3) => {
                let instruction =
                    Instruction::SYS(combine_nibbles(&[nibble1, nibble2, nibble3]) as usize);
                Ok(instruction)
            }
            (1, nibble1, nibble2, nibble3) => {
                let instruction =
                    Instruction::JP1(combine_nibbles(&[nibble1, nibble2, nibble3]) as usize);
                Ok(instruction)
            }
            (2, nibble1, nibble2, nibble3) => {
                let instruction =
                    Instruction::CALL(combine_nibbles(&[nibble1, nibble2, nibble3]) as usize);
                Ok(instruction)
            }
            (3, x, nibble1, nibble2) => {
                let instruction =
                    Instruction::SE1(x as usize, combine_nibbles(&[nibble1, nibble2]) as u8);
                Ok(instruction)
            }
            (4, x, nibble1, nibble2) => {
                let instruction =
                    Instruction::SNE1(x as usize, combine_nibbles(&[nibble1, nibble2]) as u8);
                Ok(instruction)
            }
            (5, x, y, 0) => {
                let instruction = Instruction::SE2(x as usize, y as usize);
                Ok(instruction)
            }
            (6, x, nibble1, nibble2) => {
                let instruction =
                    Instruction::LD1(x as usize, combine_nibbles(&[nibble1, nibble2]) as u8);
                Ok(instruction)
            }
            (7, x, nibble1, nibble2) => {
                let instruction =
                    Instruction::ADD1(x as usize, combine_nibbles(&[nibble1, nibble2]) as u8);
                Ok(instruction)
            }
            (8, x, y, 0) => {
                let instruction = Instruction::LD2(x as usize, y as usize);
                Ok(instruction)
            }
            (8, x, y, 1) => {
                let instruction = Instruction::OR(x as usize, y as usize);
                Ok(instruction)
            }
            (8, x, y, 2) => {
                let instruction = Instruction::AND(x as usize, y as usize);
                Ok(instruction)
            }
            (8, x, y, 3) => {
                let instruction = Instruction::XOR(x as usize, y as usize);
                Ok(instruction)
            }
            (8, x, y, 4) => {
                let instruction = Instruction::ADD2(x as usize, y as usize);
                Ok(instruction)
            }
            (8, x, y, 5) => {
                let instruction = Instruction::SUB(x as usize, y as usize);
                Ok(instruction)
            }
            (8, x, y, 6) => {
                let instruction = Instruction::SHR(x as usize, y as usize);
                Ok(instruction)
            }
            (8, x, y, 7) => {
                let instruction = Instruction::SUBN(x as usize, y as usize);
                Ok(instruction)
            }
            (8, x, y, 0xE) => {
                let instruction = Instruction::SHL(x as usize, y as usize);
                Ok(instruction)
            }
            (9, x, y, 0) => {
                let instruction = Instruction::SNE2(x as usize, y as usize);
                Ok(instruction)
            }
            (0xA, nibble1, nibble2, nibble3) => {
                let instruction =
                    Instruction::LD3(combine_nibbles(&[nibble1, nibble2, nibble3]) as usize);
                Ok(instruction)
            }
            (0xB, nibble1, nibble2, nibble3) => {
                let instruction =
                    Instruction::JP2(combine_nibbles(&[nibble1, nibble2, nibble3]) as usize);
                Ok(instruction)
            }
            (0xC, x, nibble1, nibble2) => {
                let instruction =
                    Instruction::RND(x as usize, combine_nibbles(&[nibble1, nibble2]) as u8);
                Ok(instruction)
            }
            (0xD, x, y, nibble) => {
                let instruction = Instruction::DRW(x as usize, y as usize, nibble as u8);
                Ok(instruction)
            }
            (0xE, x, 9, 0xE) => {
                let instruction = Instruction::SKP(x as usize);
                Ok(instruction)
            }
            (0xE, x, 0xA, 1) => {
                let instruction = Instruction::SKNP(x as usize);
                Ok(instruction)
            }
            (0xF, x, 0, 7) => {
                let instruction = Instruction::LD4(x as usize);
                Ok(instruction)
            }
            (0xF, x, 0, 0xA) => {
                let instruction = Instruction::LD5(x as usize);
                Ok(instruction)
            }
            (0xF, x, 1, 5) => {
                let instruction = Instruction::LD6(x as usize);
                Ok(instruction)
            }
            (0xF, x, 1, 8) => {
                let instruction = Instruction::LD7(x as usize);
                Ok(instruction)
            }
            (0xF, x, 1, 0xE) => {
                let instruction = Instruction::ADD3(x as usize);
                Ok(instruction)
            }
            (0xF, x, 2, 9) => {
                let instruction = Instruction::LD8(x as usize);
                Ok(instruction)
            }
            (0xF, x, 3, 3) => {
                let instruction = Instruction::LD9(x as usize);
                Ok(instruction)
            }
            (0xF, x, 5, 5) => {
                let instruction = Instruction::LD10(x as usize);
                Ok(instruction)
            }
            (0xF, x, 6, 5) => {
                let instruction = Instruction::LD11(x as usize);
                Ok(instruction)
            }
            _ => Err(InstructionParseError(opcode)),
        }
    }
}
