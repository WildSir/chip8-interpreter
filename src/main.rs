mod cli;
mod display;
mod game_loop;
mod instruction;
mod interpreter;
mod memory;
mod timers;
mod utils;

use clap::Parser;
use cli::Cli;
use display::{HEIGHT, WIDTH};

// TODO: Better re-exports
pub use display::{pixels::PixelsDisplayFactory, Display, DisplayMethod};
use game_loop::GameLoopOptions;
pub use interpreter::Interpreter;
pub use memory::Memory;

pub type DisplayInstruction<T> = Box<dyn FnOnce(&mut T) -> Option<bool> + Send + Sync>;

use anyhow::{Context, Result};

use std::{fs::File, process, thread};

fn main() -> Result<()> {
    env_logger::init();

    let (pixels_display, window_display) = PixelsDisplayFactory::build(HEIGHT, WIDTH, 8)?;

    let chip8_interpreter = {
        let display = Display::new(pixels_display);
        Interpreter::new(display)
    };

    let cli = Cli::parse();

    let mut file = File::open(&cli.rom_path).context("Failed to load ROM")?;
    let program_ident = format!(
        "{:x}",
        md5::compute(
            cli.rom_path
                .canonicalize()
                .unwrap()
                .to_string_lossy()
                .as_bytes()
        )
    );

    thread::spawn(move || -> Result<()> {
        if let Err(err) = game_loop::run(
            chip8_interpreter,
            GameLoopOptions {
                autosave: cli.autosave,
                save_path: cli.save_path,
                reset: cli.reset,
                clock_rate: cli.clock_rate,
                autosave_interval: cli.autosave_interval,
                program_ident,
            },
            &mut file,
        ) {
            eprintln!("{}", err);
            process::exit(1);
        };
        Ok(())
    });

    window_display
        .execute()
        .context("Failed to create window")?;
    Ok(())
}
