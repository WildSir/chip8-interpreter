pub fn as_bcd(mut byte: u8) -> [u8; 3] {
    let mut bcd = [0; 3];
    for num in &mut bcd {
        *num = byte % 10;
        byte /= 10;
    }
    bcd.reverse();
    bcd
}

pub fn split_nibbles(num: u16) -> (u16, u16, u16, u16) {
    (
        (num & 0xF000) >> 12,
        (num & 0xF00) >> 8,
        (num & 0xF0) >> 4,
        num & 0xF,
    )
}

pub fn combine_nibbles(nibbles: &[u16]) -> u16 {
    if nibbles.len() == 4 {
        (nibbles[0] << 12) ^ (nibbles[1] << 8) ^ (nibbles[2] << 4) ^ nibbles[3]
    } else if nibbles.len() == 3 {
        (nibbles[0] << 8) ^ (nibbles[1] << 4) ^ nibbles[2]
    } else if nibbles.len() == 2 {
        (nibbles[0] << 4) ^ nibbles[1]
    } else {
        panic!("Error: nibble array must be at least two nibbles or a max of 4");
    }
}

pub fn toggle_pixel(pixel: &mut [u8], state: bool) {
    if state {
        pixel.copy_from_slice(&[0xff, 0xff, 0xff, 0xff]);
    } else {
        pixel.copy_from_slice(&[0x0, 0x0, 0x0, 0x0]);
    }
}
