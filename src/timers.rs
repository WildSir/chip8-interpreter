use serde::{Deserialize, Serialize};

#[derive(Default, Debug, Serialize, Deserialize, Clone, Copy)]
pub struct SoundTimer {
    pub x: u8,
}

impl SoundTimer {
    fn play_sound(&mut self) {
        println!("\x07 \x1b[1A");
    }
    pub fn is_active(&self) -> bool {
        self.x > 0
    }
    pub fn tick(&mut self) {
        if self.is_active() {
            self.play_sound();
            self.x -= 1;
        }
    }
}

#[derive(Default, Debug, Serialize, Deserialize, Clone, Copy)]
pub struct DelayTimer {
    pub x: u8,
}

impl DelayTimer {
    pub fn is_active(&self) -> bool {
        self.x > 0
    }
    pub fn tick(&mut self) {
        if self.is_active() {
            self.x -= 1;
        }
    }
}
