use anyhow::Result;
use log::{error, info};
use std::{
    fs,
    io::{ErrorKind, Read},
    path::PathBuf,
    time::Instant,
};

use crate::{
    interpreter::{ControlFlow, Interpreter, InterpreterError},
    DisplayMethod,
};

const MAX_FRAMESKIP: u32 = 10;
const REFRESH_DELTA: f64 = 1.0 / 60.0;

pub struct GameLoopOptions {
    pub autosave: bool,
    pub save_path: Option<PathBuf>,
    pub clock_rate: u32,
    pub autosave_interval: u32,
    pub reset: bool,
    pub program_ident: String,
}

pub fn run<T: DisplayMethod + std::fmt::Debug + Send + Sync + 'static>(
    mut interpreter: Interpreter<T>,
    options: GameLoopOptions,
    program: &mut impl Read,
) -> Result<()> {
    let start = Instant::now();
    let skip_ticks = 1000 / options.clock_rate;

    let mut last_frame = start;
    let mut last_autosave = start;

    let mut loops: u32;
    let mut next_game_tick = 0;
    let mut needs_refresh = false;

    let save_path = options
        .save_path
        .unwrap_or(PathBuf::from(options.program_ident + ".json"));

    interpreter.load_program(program)?;

    if !options.reset {
        interpreter = match interpreter.load_state(&save_path) {
            Ok(interpreter) => interpreter,
            Err(InterpreterError::Load(err, interpreter)) => match err.kind() {
                ErrorKind::NotFound => {
                    interpreter.save_state(&save_path)?;
                    interpreter
                }
                _ => return Err(err.into()),
            },
            Err(err) => return Err(err.into()),
        }
    } else {
        match fs::remove_file(&save_path) {
            Ok(()) => {}
            Err(err) => match err.kind() {
                ErrorKind::NotFound => {}
                _ => return Err(err.into()),
            },
        }
    }

    'outer: loop {
        let now = Instant::now();
        if options.autosave
            && now.duration_since(last_autosave).as_secs() as u32 >= options.autosave_interval
        {
            info!("Autosaving...");
            interpreter.save_state(&save_path)?;
            last_autosave = now;
        }

        if now.duration_since(last_frame).as_secs_f64() >= REFRESH_DELTA {
            if needs_refresh {
                interpreter.display.render()?;
                needs_refresh = false;
            }
            interpreter.tick_timers();
            last_frame = now;
        }
        loops = 0;
        while start.elapsed().as_millis() > next_game_tick && loops < MAX_FRAMESKIP {
            match interpreter.run_instruction() {
                Ok(ControlFlow::End) => {
                    break 'outer;
                }
                Ok(ControlFlow::Continue) => {}
                Ok(ControlFlow::RefreshDisplay) => needs_refresh = true,
                Err(err) => {
                    error!("{err}");
                    break 'outer;
                }
            }
            next_game_tick += skip_ticks as u128;
            loops += 1;
        }
    }
    // Will not be reached currently
    info!("Program has finished running");
    Ok(())
}
