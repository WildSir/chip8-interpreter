use std::path::PathBuf;

use clap::Parser;

#[derive(Parser)]
pub struct Cli {
    /// File path for ROM
    pub rom_path: PathBuf,
    #[arg(short, long)]
    /// Whether the game should autosave at a constant interval
    pub autosave: bool,
    #[arg(env)]
    /// Where the ROM save file should be stored
    pub save_path: Option<PathBuf>,
    #[arg(short, long)]
    /// Reset ROM save file
    pub reset: bool,
    #[arg(long, default_value = "500")]
    /// How many CPY cycles per second
    pub clock_rate: u32,
    #[clap(env, default_value = "10", value_parser = clap::value_parser!(u32).range(1..))]
    //// How many times to autosave per minute. May negatively impact performance if too low
    pub autosave_interval: u32,
    // TODO: BG and FG support
    // #[clap(long)]
    // pub bg: u8,
    // #[clap(long)]
    // pub fg: u8,

    // #[arg(short, long)]
    // debug: bool,
}
