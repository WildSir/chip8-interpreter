pub mod pixels;

pub const WIDTH: usize = 64;
pub const HEIGHT: usize = 32;

use std::{
    collections::HashSet,
    error,
    ops::{Deref, DerefMut},
};

pub trait DisplayMethod {
    type Err: error::Error + Send + Sync + 'static;

    fn render(&mut self) -> Result<(), Self::Err>;
    fn draw_sprite(&mut self, bytes: &[u8], x: usize, y: usize) -> bool;
    fn clear(&mut self);
    fn read_keys(&mut self) -> HashSet<u8>;
    fn get_screen_buf(&self) -> &[u8];
    fn load_screen_buf(&mut self, buf: &[u8]);
}

#[derive(Debug, Default)]
pub struct Display<T> {
    method: T,
}
impl<T: DisplayMethod> Display<T> {
    pub fn new(method: T) -> Self {
        Display { method }
    }
}

impl<T: DisplayMethod> Deref for Display<T> {
    type Target = T;
    fn deref(&self) -> &Self::Target {
        &self.method
    }
}
impl<T: DisplayMethod> DerefMut for Display<T> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.method
    }
}
