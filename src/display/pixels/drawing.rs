use pixels::{Pixels, SurfaceTexture};

use std::{
    collections::HashSet,
    sync::{Arc, Mutex},
};

use winit::window::Window;

use crate::{utils, DisplayMethod};

use super::{super::HEIGHT, super::WIDTH, PixelsError};

#[derive(Debug)]
pub struct PixelsDrawing {
    pixels: Pixels,
    width: usize,
    height: usize,
    keybr_input: Arc<Mutex<HashSet<u8>>>,
}
impl PixelsDrawing {
    pub fn build(
        height: usize,
        width: usize,
        window: &Window,
        keybr_input: Arc<Mutex<HashSet<u8>>>,
    ) -> Result<Self, PixelsError> {
        let pixels = {
            let window_size = window.inner_size();
            let surface_texture =
                SurfaceTexture::new(window_size.width, window_size.height, &window);
            Pixels::new(WIDTH as u32, HEIGHT as u32, surface_texture)?
        };
        Ok(PixelsDrawing {
            pixels,
            width,
            height,
            keybr_input,
        })
    }
    pub fn width(&self) -> usize {
        self.width
    }
    pub fn height(&self) -> usize {
        self.height
    }
}

impl PixelsDrawing {
    fn xor_pixel(&mut self, mut x: usize, mut y: usize, other: bool) -> bool {
        if x > self.width() - 1 {
            x %= 64;
        }
        if y > self.height() - 1 {
            y %= 32;
        }

        let starting_index = 4 * (y * self.width + x);
        let pixel: &mut [u8] = &mut self.pixels.frame_mut()[starting_index..starting_index + 4];
        let mut byte_state = false;
        if *pixel == [0xff, 0xff, 0xff, 0xff] {
            byte_state = true;
        }
        let new_byte_state = byte_state ^ other;
        utils::toggle_pixel(pixel, new_byte_state);
        !new_byte_state && byte_state
    }
}

impl DisplayMethod for PixelsDrawing {
    type Err = PixelsError;
    fn render(&mut self) -> Result<(), Self::Err> {
        self.pixels.render()?;
        Ok(())
    }
    fn get_screen_buf(&self) -> &[u8] {
        self.pixels.frame()
    }
    fn load_screen_buf(&mut self, buf: &[u8]) {
        self.pixels.frame_mut()[..].copy_from_slice(buf);
    }
    fn draw_sprite(&mut self, sprite_data: &[u8], x: usize, y: usize) -> bool {
        let mut collision = false;
        for (row, byte) in sprite_data.iter().enumerate() {
            let byte = byte.reverse_bits();
            for column in 0..=7 {
                let other = byte & (1 << column) != 0;
                if other && self.xor_pixel(x + column, y + row, other) {
                    collision = true;
                }
                if (x + column) % 64 == self.width() - 1 {
                    break;
                }
            }
            if (y + row) % 32 == self.height() - 1 {
                break;
            }
        }
        collision
    }
    fn read_keys(&mut self) -> HashSet<u8> {
        self.keybr_input.lock().unwrap().clone()
    }
    fn clear(&mut self) {
        self.pixels.frame_mut().fill(0);
    }
}

// impl Serialize for PixelsDrawing {
//     fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
//         where
//             S: serde::Serializer {
//         let mut state = serializer.serialize_struct("PixelsDrawing", 4)?;

//         todo!()
//     }
// }
