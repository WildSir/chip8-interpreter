use std::{
    collections::HashSet,
    sync::{Arc, Mutex},
};

use super::PixelsError;
use winit::{
    dpi::LogicalSize,
    error::EventLoopError,
    event::{Event, WindowEvent},
    event_loop::{ControlFlow, EventLoop},
    keyboard::{KeyCode, PhysicalKey},
    window::{Window, WindowBuilder},
};

#[derive(Debug)]
pub struct WindowDisplay {
    window: Window,
    event_loop: EventLoop<()>,
    keybr_input: Arc<Mutex<HashSet<u8>>>,
}

impl WindowDisplay {
    pub fn build(height: usize, width: usize, scale_factor: usize) -> Result<Self, PixelsError> {
        let event_loop = EventLoop::new()?;

        let window = {
            let size = LogicalSize::new(
                (width * scale_factor) as u32,
                (height * scale_factor) as u32,
            );

            WindowBuilder::new()
                .with_title("Chip-8 Interpreter")
                .with_inner_size(size)
                .with_min_inner_size(size)
                .build(&event_loop)?
        };

        event_loop.set_control_flow(ControlFlow::Poll);
        Ok(WindowDisplay {
            keybr_input: Arc::new(Mutex::new(HashSet::new())),
            event_loop,
            window,
        })
    }
    pub fn window(&self) -> &Window {
        &self.window
    }
    pub fn keybr_input(&self) -> Arc<Mutex<HashSet<u8>>> {
        Arc::clone(&self.keybr_input)
    }
    pub fn execute(self) -> Result<(), EventLoopError> {
        self.event_loop.run(move |event, elwt| {
            match event {
                Event::WindowEvent {
                    event: WindowEvent::CloseRequested,
                    ..
                } => elwt.exit(),
                Event::WindowEvent {
                    event: WindowEvent::KeyboardInput { event, .. },
                    ..
                } => {
                    if let PhysicalKey::Code(key_code) = event.physical_key {
                        let key = match key_code {
                            KeyCode::Digit1 => 1,
                            KeyCode::Digit2 => 2,
                            KeyCode::Digit3 => 3,
                            KeyCode::Digit4 => 12,
                            KeyCode::KeyQ => 4,
                            KeyCode::KeyW => 5,
                            KeyCode::KeyE => 6,
                            KeyCode::KeyR => 13,
                            KeyCode::KeyA => 7,
                            KeyCode::KeyS => 8,
                            KeyCode::KeyD => 9,
                            KeyCode::KeyF => 14,
                            KeyCode::KeyZ => 10,
                            KeyCode::KeyX => 0,
                            KeyCode::KeyC => 11,
                            KeyCode::KeyV => 15,
                            _ => return,
                        };
                        let mut keybr_input = self.keybr_input.lock().unwrap();
                        if event.state.is_pressed() {
                            keybr_input.insert(key);
                        } else {
                            keybr_input.remove(&key);
                        }
                    };
                }
                Event::WindowEvent {
                    event: WindowEvent::RedrawRequested,
                    ..
                } => {
                    self.window.request_redraw();
                }
                _ => (),
            };
        })
    }
}
