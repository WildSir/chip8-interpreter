pub mod drawing;
pub mod window;
use std::sync::mpsc::RecvError;

pub use self::{drawing::PixelsDrawing, window::WindowDisplay};

use thiserror::Error;
use winit::error::{EventLoopError, OsError};

#[derive(Error, Debug)]
pub enum PixelsError {
    #[error("failed to create event loop")]
    EventLoop(#[from] EventLoopError),
    #[error("failed to create window")]
    Os(#[from] OsError),
    #[error("failed to create pixel buffer")]
    Pixel(#[from] pixels::Error),
    #[error("failed to read keys from window")]
    Recv(#[from] RecvError),
}

#[derive(Debug)]
pub struct PixelsDisplayFactory;
impl PixelsDisplayFactory {
    pub fn build(
        height: usize,
        width: usize,
        scale_factor: usize,
    ) -> Result<(PixelsDrawing, WindowDisplay), PixelsError> {
        let window_display = WindowDisplay::build(height, width, scale_factor)?;
        let pixels_display = PixelsDrawing::build(
            height,
            width,
            window_display.window(),
            window_display.keybr_input(),
        )?;

        Ok((pixels_display, window_display))
    }
}
